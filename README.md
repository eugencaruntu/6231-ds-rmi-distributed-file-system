# Lab3: DFS Assignment
A distributed file system: client-server communication through Remote Method Invocation (RMI)

Eugen Caruntu, ID 29077103, COMP 6231 LabAssignment 3

##### Functionalities implemented:
1. **`CreateFile(path)`**: create the file referred to by path 1 .
2. **`CreateDirectory(path)`**: create the directory referred to by path.
3. **`Read(path, o, n)`**: read n bytes of data from the file referred to by path starting at an offset o.
4. **`Write(path, o, data)`**: write n bytes of data to the file referred to by path starting at an offset o.
5. **`Size(path)`**: return the size, in bytes, of the file referred to by path.
6. **`IsDirectory(path)`**: return true if path refers to a directory.
7. **`List(path)`**: list the contents of the directory referred to by path.
8. **`Delete(path)`**: delete the file or directory referred to by path.
9. **`GetStorage(path)`**: get the Storage Server (or more precisely, a representing stub) hosting the file referred to by path.
10. **THE BONUS** **`Delete(path)`**:  deletes a file or directory on the storage server referred to it by path (recursive implementation)
