package naming;

/**
 * A simplistic lock mechanism to protect access when writing
 */
class WriteLock {
    private volatile boolean writeLock = false;

    /**
     * Notifies all threads when interrupted
     */
    synchronized void interrupt() {
        notifyAll();
    }

    /**
     * Aquire write lock
     *
     * @throws InterruptedException if interrupted
     */
    synchronized void lockWrite() throws InterruptedException {
        while (writeLock) {
            wait();
        }
        writeLock = true;
    }

    /**
     * Release the write lock
     */
    synchronized void unlockWrite() {
        writeLock = false;
        notifyAll();
    }

}
