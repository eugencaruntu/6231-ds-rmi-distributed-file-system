package naming;

import common.Path;
import rmi.RMIException;
import rmi.Skeleton;
import storage.Command;
import storage.Storage;

import java.io.FileNotFoundException;
import java.net.InetSocketAddress;
import java.util.*;
import java.util.concurrent.ConcurrentHashMap;

/**
 * Naming server.
 *
 * <p>
 * Each instance of the filesystem is centered on a single naming server. The
 * naming server maintains the filesystem directory tree. It does not store any
 * file data - this is done by separate storage servers. The primary purpose of
 * the naming server is to map each file name (path) to the storage server
 * which hosts the file's contents.
 *
 * <p>
 * The naming server provides two interfaces, <code>Service</code> and
 * <code>Registration</code>, which are accessible through RMI. Storage servers
 * use the <code>Registration</code> interface to inform the naming server of
 * their existence. Clients use the <code>Service</code> interface to perform
 * most filesystem operations. The documentation accompanying these interfaces
 * provides details on the methods supported.
 *
 * <p>
 * Stubs for accessing the naming server must typically be created by directly
 * specifying the remote network address. To make this possible, the client and
 * registration interfaces are available at well-known ports defined in
 * <code>NamingStubs</code>.
 */
public class NamingServer implements Service, Registration {

    private final Skeleton<Registration> registrationSkeleton;
    private final Skeleton<Service> serviceSkeleton;
    private ConcurrentHashMap<Path, WriteLock> locks;

    private ConcurrentHashMap<Path, Set<Path>> paths;
    private ConcurrentHashMap<Path, Set<Storage>> storage;
    private ConcurrentHashMap<Storage, Command> registeredStorageServers;

    /**
     * Creates the naming server object.
     *
     * <p>
     * The naming server is not started.
     */
    public NamingServer() {

        paths = new ConcurrentHashMap<>();
        storage = new ConcurrentHashMap<>();
        registeredStorageServers = new ConcurrentHashMap<>();
        locks = new ConcurrentHashMap<>();
        locks.put(new Path(), new WriteLock());

        InetSocketAddress naminServiceAddress = new InetSocketAddress(NamingStubs.SERVICE_PORT);
        serviceSkeleton = new Skeleton<>(Service.class, this, naminServiceAddress);

        InetSocketAddress registrationAddress = new InetSocketAddress(NamingStubs.REGISTRATION_PORT);
        registrationSkeleton = new Skeleton<>(Registration.class, this, registrationAddress);
    }

    /**
     * Starts the naming server.
     *
     * <p>
     * After this method is called, it is possible to access the client and
     * registration interfaces of the naming server remotely.
     *
     * @throws RMIException If either of the two skeletons, for the client or
     *                      registration server interfaces, could not be
     *                      started. The user should not attempt to start the
     *                      server again if an exception occurs.
     */
    public synchronized void start() throws RMIException {
        serviceSkeleton.start();
        registrationSkeleton.start();
    }

    /**
     * Stops the naming server.
     *
     * <p>
     * This method waits for both the client and registration interface
     * skeletons to stop. It attempts to interrupt as many of the threads that
     * are executing naming server code as possible. After this method is
     * called, the naming server is no longer accessible remotely. The naming
     * server should not be restarted.
     */
    public void stop() {
        serviceSkeleton.stop();
        synchronized (serviceSkeleton) {
            try {
                serviceSkeleton.wait();
            } catch (InterruptedException ignored) {
            }
        }

        registrationSkeleton.stop();
        synchronized (registrationSkeleton) {
            try {
                registrationSkeleton.wait();
            } catch (InterruptedException ignored) {
            }
        }

        for (WriteLock lock : locks.values()) {
            lock.interrupt();
        }

        stopped(null);

    }

    /**
     * Indicates that the server has completely shut down.
     *
     * <p>
     * This method should be overridden for error reporting and application
     * exit purposes. The default implementation does nothing.
     *
     * @param cause The cause for the shutdown, or <code>null</code> if the
     *              shutdown was by explicit user request.
     */
    protected void stopped(Throwable cause) {
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #6: return true if path refers to a directory
     * {@inheritDoc}
     */
    @Override
    public boolean isDirectory(Path path) throws FileNotFoundException {
        if (path == null) {
            throw new NullPointerException();
        }

        boolean directory = paths.containsKey(path);
        if (!directory && !storage.containsKey(path)) {
            throw new FileNotFoundException();
        }
        return directory;
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #7: list the contents of the directory referred to by path
     * {@inheritDoc}
     */
    @Override
    public String[] list(Path directory) throws FileNotFoundException {
        if (!isDirectory(directory)) {
            throw new FileNotFoundException();
        }
        Set<Path> directoryPaths = paths.get(directory);
        return directoryPaths.stream().map(Path::last).toArray(String[]::new);
    }

    /**
     * Helper method to randomly select an element from a collection
     * In our case, we need to have a way to get a random element from a set
     *
     * @param collection the set
     * @param <T>        the type
     * @return a random element of type T
     */
    private static <T> T getRandomKeyFromSet(Collection<T> collection) {
        int item = new Random().nextInt(collection.size());
        int i = 0;
        for (Object obj : collection) {
            if (i == item) {
                return (T) obj;
            }
            i++;
        }

        return null;
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #1: create the file referred by path
     * {@inheritDoc}
     */
    @Override
    public boolean createFile(Path file) throws RMIException, IllegalStateException, FileNotFoundException {

        if (!file.isRoot() && !paths.containsKey(file.parent())) {
            throw new FileNotFoundException();
        }

        if (registeredStorageServers.isEmpty()) {
            throw new IllegalStateException();
        }

        boolean fileCreated = false;
        if (!file.isRoot() && !paths.containsKey(file) && !storage.containsKey(file)) {

            Storage storageStub = getRandomKeyFromSet(registeredStorageServers.keySet());
            Command commandStub = registeredStorageServers.get(Objects.requireNonNull(storageStub));

            fileCreated = commandStub.create(file);

            if (fileCreated) {
                updatePaths(file);
                Set<Storage> storageSet = Collections.newSetFromMap(new ConcurrentHashMap<>());
                storageSet.add(storageStub);
                storage.put(file, storageSet);
            }
        }

        return fileCreated;
    }

    /**
     * Updates the directory tree when folder/files are created or registered
     *
     * @param file the path to be added
     */
    private void updatePaths(Path file) {
        Path child = file;

        if (!locks.containsKey(child)) {
            locks.put(child, new WriteLock());
        }

        while (!child.isRoot()) {
            Path parent = child.parent();
            if (!locks.containsKey(parent)) {
                locks.put(parent, new WriteLock());
            }
            if (paths.containsKey(parent)) {
                paths.get(parent).add(child);
            } else {
                Set<Path> contentsSet = Collections.newSetFromMap(new ConcurrentHashMap<>());
                contentsSet.add(child);
                paths.put(parent, contentsSet);
            }
            child = parent;
        }
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #2: create the directory referred by path
     * {@inheritDoc}
     */
    @Override
    public boolean createDirectory(Path directory) throws FileNotFoundException {
        if (!directory.isRoot() && !paths.containsKey(directory.parent())) {
            throw new FileNotFoundException();
        }
        boolean directoryCreated = false;
        if (!directory.isRoot() && !paths.containsKey(directory) && !storage.containsKey(directory)) {
            updatePaths(directory);
            Set<Path> directoryContents = Collections.newSetFromMap(new ConcurrentHashMap<Path, Boolean>());
            paths.put(directory, directoryContents);
            directoryCreated = true;
        }

        return directoryCreated;
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #8: delete the file or directory referred to by path
     * {@inheritDoc}
     */
    @Override
    public boolean delete(Path path) throws FileNotFoundException, RMIException {
        if (path == null) {
            throw new NullPointerException();
        } else if (path.isRoot()) {
            return false;
        } else if (!locks.containsKey(path) || !locks.containsKey(path.parent())) {
            throw new FileNotFoundException();
        }

        boolean deleted = false;
        Set<Storage> storageLocations = registeredStorageServers.keySet();
        for (Storage storage : storageLocations) {
            Command commandStub = registeredStorageServers.get(storage);
            deleted = commandStub.delete(path) || deleted;
        }
        removeUnused(path);
        paths.get(path.parent()).remove(path);

        return deleted;
    }

    /**
     * Remove unused paths from a directory tree when deletion happens
     *
     * @param path the path to be removed
     */
    private void removeUnused(Path path) {

        if (!paths.containsKey(path)) {
            storage.remove(path);
        } else {
            Set<Path> directoryContents = paths.get(path);
            for (Path p : directoryContents) {
                removeUnused(p);
            }
            paths.remove(path);
        }

        locks.remove(path);
    }

    /**
     * Implementation for method declared in Service.java
     * Functionality #9: get the Storage Server (or more precisely, a representing stub) hosting the file referred to by path
     * {@inheritDoc}
     */
    @Override
    public Storage getStorage(Path file) throws FileNotFoundException {
        if (!storage.containsKey(file)) {
            throw new FileNotFoundException();
        }

        return getRandomKeyFromSet(storage.get(file));
    }

    /**
     * Implementation for method declared in Registration.java
     * {@inheritDoc}
     */
    @Override
    public Path[] register(Storage client_stub, Command command_stub, Path[] files) {

        if (client_stub == null || command_stub == null || files == null) {
            throw new NullPointerException();
        }

        if (registeredStorageServers.containsKey(client_stub)) {
            throw new IllegalStateException();
        } else {
            registeredStorageServers.put(client_stub, command_stub);
        }

        ArrayList<Path> filesToDelete = new ArrayList<>();
        for (Path path : files) {
            if (path.isRoot()) {
//                System.out.println("We cant add root as file");
            } else if (paths.containsKey(path) || storage.containsKey(path)) {
                filesToDelete.add(path);
            } else {
                Set<Storage> storageList = Collections.newSetFromMap(new ConcurrentHashMap<>());
                storageList.add(client_stub);
                storage.put(path, storageList);

                try {
                    locks.get(new Path()).lockWrite();
                    updatePaths(path);
                    locks.get(new Path()).unlockWrite();
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }

            }
        }

        Path[] duplicatePaths = new Path[filesToDelete.size()];
        filesToDelete.toArray(duplicatePaths);

        return duplicatePaths;
    }
}