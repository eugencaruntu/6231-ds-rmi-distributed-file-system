package storage;

import common.Path;
import naming.Registration;
import rmi.RMIException;
import rmi.Skeleton;
import rmi.Stub;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.net.InetSocketAddress;

/**
 * Storage server.
 *
 * <p>
 * Storage servers respond to client file access requests. The files accessible
 * through a storage server are those accessible under a given directory of the
 * local filesystem.
 */
public class StorageServer implements Storage, Command {
	private Skeleton<Storage> storageSkeleton;
	private Skeleton<Command> commandSkeleton;
    private File root;

	/**
	 * Creates a storage server, given a directory on the local filesystem, and
	 * ports to use for the client and command interfaces.
	 *
	 * <p>
	 * The ports may have to be specified if the storage server is running
	 * behind a firewall, and specific ports are open.
	 *
	 * @param root         Directory on the local filesystem. The contents of this
	 *                     directory will be accessible through the storage server.
	 * @param client_port  Port to use for the client interface, or zero if the
	 *                     system should decide the port.
	 * @param command_port Port to use for the command interface, or zero if
	 *                     the system should decide the port.
	 * @throws NullPointerException If <code>root</code> is <code>null</code>.
	 */
	private StorageServer(File root, int client_port, int command_port) {
		if (root == null) {
			throw new NullPointerException();
		}
		this.root = root;
		InetSocketAddress storageAddr = new InetSocketAddress(client_port);
		storageSkeleton = new Skeleton<>(Storage.class, this, storageAddr);

		InetSocketAddress commandAddr = new InetSocketAddress(command_port);
		commandSkeleton = new Skeleton<>(Command.class, this, commandAddr);
	}

	/**
	 * Creats a storage server, given a directory on the local filesystem.
	 *
	 * <p>
	 * This constructor is equivalent to
	 * <code>StorageServer(root, 0, 0)</code>. The system picks the ports on
	 * which the interfaces are made available.
	 *
	 * @param root Directory on the local filesystem. The contents of this
	 *             directory will be accessible through the storage server.
	 * @throws NullPointerException If <code>root</code> is <code>null</code>.
	 */
	public StorageServer(File root) {
		this(root, 0, 0);
	}

	/**
	 * Starts the storage server and registers it with the given naming
	 * server.
	 *
	 * @param hostname      The externally-routable hostname of the local host on
	 *                      which the storage server is running. This is used to
	 *                      ensure that the stub which is provided to the naming
	 *                      server by the <code>start</code> method carries the
	 *                      externally visible hostname or address of this storage
	 *                      server.
	 * @param naming_server Remote interface for the naming server with which
	 *                      the storage server is to register.
	 * @throws FileNotFoundException If the directory with which the server was
	 *                               created does not exist or is in fact a
	 *                               file.
	 * @throws RMIException          If the storage server cannot be started, or if it
	 *                               cannot be registered.
	 */
	public synchronized void start(String hostname, Registration naming_server) throws RMIException, FileNotFoundException {

		this.commandSkeleton.start();
		this.storageSkeleton.start();

		Storage storageStub = Stub.create(Storage.class, this.storageSkeleton, hostname);
		Command commandStub = Stub.create(Command.class, this.commandSkeleton, hostname);

		Path[] duplicates = naming_server.register(storageStub, commandStub, Path.list(this.root));

		for (Path p : duplicates) {
			this.delete(p);
		}

		pruneEmptyDirectories(this.root);
	}

	/**
	 * Deletes empty folders in directory tree
	 *
	 * @param root the root in the directory tree
	 * @return true or false depending on the deletion result
	 */
	private boolean pruneEmptyDirectories(File root) {
		boolean toDelete = true;

		File[] contents = root.listFiles();
		if (contents == null) {
			toDelete = true;
		} else {
			for (File f : contents) {
				if (f.isDirectory()) {
					toDelete = toDelete && pruneEmptyDirectories(f);
				}
			}
		}

		if (toDelete) {
			root.delete();
		}

		return toDelete;
	}

	/**
	 * Stops the storage server.
	 *
	 * <p>
	 * The server should not be restarted.
	 */
	public void stop() {
		commandSkeleton.stop();
		synchronized (commandSkeleton) {
			try {
				commandSkeleton.wait();
			} catch (InterruptedException ignored) {
			}
		}

		storageSkeleton.stop();
		synchronized (storageSkeleton) {
			try {
				storageSkeleton.wait();
			} catch (InterruptedException ignored) {
			}
		}

		this.stopped(null);
	}

	/**
	 * Called when the storage server has shut down.
	 *
	 * @param cause The cause for the shutdown, if any, or <code>null</code> if
	 *              the server was shut down by the user's request.
	 */
	protected void stopped(Throwable cause) {
    }


	/**
	 * Implementation for method declared in Storage.java
     * Functionality #3: read n bytes of data from the file referred by path starting at an offset, up to a length
	 * {@inheritDoc}
	 */
	@Override
	public synchronized byte[] read(Path file, long offset, int length) throws IOException {
		File f = file.toFile(this.root);

		if (!f.exists() || f.isDirectory()) {
			throw new FileNotFoundException();
		}

		if (offset + length > f.length() || offset < 0 || length < 0) {
			throw new IndexOutOfBoundsException();
		}

		byte[] data = new byte[length];
		RandomAccessFile reader = new RandomAccessFile(f, "r");
		reader.seek(offset);
		reader.read(data, 0, length);
		reader.close();

		return data;
	}

	/**
	 * Implementation for method declared in Storage.java
     * Functionality #4: write n bytes of data to the file referred by the path starting at an offset
	 * {@inheritDoc}
	 */
	@Override
	public synchronized void write(Path file, long offset, byte[] data) throws IOException {
		File f = file.toFile(this.root);

		if (!f.exists() || f.isDirectory()) {
			throw new FileNotFoundException();
		}

		if (offset < 0) {
			throw new IndexOutOfBoundsException();
		}

		RandomAccessFile writer = new RandomAccessFile(f, "rw");

		writer.seek(offset);
		writer.write(data);
		writer.close();

    }

    /**
     * Implementation for method declared in Storage.java
     * Functionality #5: return the size, in bytes, of the file referred to by path
     * {@inheritDoc}
     */
    @Override
    public synchronized long size(Path file) throws FileNotFoundException {
        File f = file.toFile(this.root);

        if (!f.exists() || f.isDirectory()) {
            throw new FileNotFoundException();
        }

        long size = f.length();
        return size;
    }

	/**
	 * Implementation for method declared in Command.java
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean create(Path file) {
		if (file.isRoot()) {
			return false;
		}

		File f = file.toFile(root);

		Path parent = file.parent();
		if (!parent.toFile(root).exists()) {
			parent.toFile(root).mkdirs();
		}

		try {
			return f.createNewFile();
		} catch (IOException ignored) {
		}

		return false;
	}

	/**
	 * Implementation for method declared in Command.java
     * Functionality #BONUS: deletes a file or directory on the storage server referred to it by path
	 * {@inheritDoc}
	 */
	@Override
	public synchronized boolean delete(Path path) {
		if (path.isRoot()) {
			return false;
        }

        return this.delete(path.toFile(root)); // uses the recursive implementation of the bonus delete function
    }

    /**
     * The recursive implementation of the bonus delete function
     * Deletes a file or folder if there are no more files within
	 * It is executed recursively
	 *
	 * @param file the folder
	 * @return true or false depending on the result
	 */
	private boolean delete(File file) {
		boolean allFilesDeleted = true;
		if (file.isDirectory()) {
			for (File f : file.listFiles()) {
				//Recursively delete all files in subdirectories
				allFilesDeleted = this.delete(f) && allFilesDeleted;
			}
			if (allFilesDeleted) {
				file.delete();
			}
		} else {
			allFilesDeleted = file.delete();
		}

		return allFilesDeleted;
    }

}