package common;


import java.io.File;
import java.io.FileNotFoundException;
import java.io.Serializable;
import java.util.Iterator;
import java.util.Objects;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * Distributed filesystem paths.
 *
 * <p>
 * Objects of type <code>Path</code> are used by all filesystem interfaces.
 * Path objects are immutable.
 *
 * <p>
 * The string representation of paths is a forward-slash-delimited sequence of
 * path components. The root directory is represented as a single forward
 * slash.
 *
 * <p>
 * The colon (<code>:</code>) and forward slash (<code>/</code>) characters are
 * not permitted within path components. The forward slash is the delimiter,
 * and the colon is reserved as a delimiter for application use.
 */
public class Path implements Iterable<String>, Serializable {
    private Vector<String> pathVector;

    /**
     * Creates a new path which represents the root directory.
     */
    public Path() {
        pathVector = new Vector<>();
    }

    /**
     * Creates a new path by appending the given component to an existing path.
     *
     * @param path      The existing path.
     * @param component The new component.
     * @throws IllegalArgumentException If <code>component</code> includes the
     *                                  separator, a colon, or
     *                                  <code>component</code> is the empty
     *                                  string.
     */
    public Path(Path path, String component) {
        // delegate to other constructor once path is converted toString
        this(path.toString());
        if (component.equals("") || component.contains("/") || component.contains(":")) {
            throw new IllegalArgumentException();
        }
        pathVector.add(component);
    }

    /**
     * Creates a new path from a path string.
     *
     * <p>
     * The string is a sequence of components delimited with forward slashes.
     * Empty components are dropped. The string must begin with a forward
     * slash.
     *
     * @param path The path string.
     * @throws IllegalArgumentException If the path string does not begin with
     *                                  a forward slash, or if the path
     *                                  contains a colon character.
     */
    public Path(String path) {
        if (!path.startsWith("/") || path.contains(":")) {
            throw new IllegalArgumentException();
        }

        pathVector = new Vector<>();
        StringTokenizer stringTokenizer = new StringTokenizer(path, "/");
        while (stringTokenizer.hasMoreTokens()) {
            pathVector.add(stringTokenizer.nextToken());
        }
    }

    /**
     * Returns an iterator over the components of the path.
     *
     * <p>
     * The iterator cannot be used to modify the path object - the
     * <code>remove</code> method is not supported.
     *
     * @return The iterator.
     */
    @Override
    public Iterator<String> iterator() {
        return new VectorIterator(this.pathVector.iterator());
    }

    /**
     * Lists the paths of all files in a directory tree on the local
     * filesystem.
     *
     * @param directory The root directory of the directory tree.
     * @return An array of relative paths, one for each file in the directory
     * tree.
     * @throws FileNotFoundException    If the root directory does not exist.
     * @throws IllegalArgumentException If <code>directory</code> exists but
     *                                  does not refer to a directory.
     */
    public static Path[] list(File directory) throws FileNotFoundException {
        if (!directory.exists()) {
            throw new FileNotFoundException("The root directory does not exist");
        }

        if (!directory.isDirectory()) {
            throw new IllegalArgumentException("The directory is not a folder");
        }

        return directoryContents(directory).toArray(new Path[0]);
    }

    /**
     * Helper to navigate directory structure
     *
     * @param directory the folder in which we look for all paths
     * @return an array
     * @throws FileNotFoundException when the file is does not exist
     */
    private static Vector<Path> directoryContents(File directory) throws FileNotFoundException {
        Vector<Path> allPaths = new Vector<>();
        for (File file : Objects.requireNonNull(directory.listFiles())) {
            if (!file.isDirectory()) { // if file
                allPaths.add(new Path("/" + file.getName()));
            } else { // if node/folder call the list() recursively
                Path[] subfolders = Path.list(file);
                for (Path subfolder : subfolders) {
                    allPaths.add(new Path("/" + file.getName() + subfolder.toString()));
                }
            }
        }
        return allPaths;
    }

    /**
     * Determines whether the path represents the root directory.
     *
     * @return <code>true</code> if the path does represent the root directory,
     * and <code>false</code> if it does not.
     */
    public boolean isRoot() {
        return pathVector.isEmpty();
    }

    /**
     * Returns the path to the parent of this path.
     *
     * @return the path to the parent of this path
     * @throws IllegalArgumentException If the path represents the root
     *                                  directory, and therefore has no parent.
     */
    public Path parent() {
        if (!isRoot()) {
            String last = pathVector.lastElement();
            pathVector.remove(pathVector.lastElement());
            String parentPath = toString();
            pathVector.add(last);
            return new Path(parentPath);
        } else {
            throw new IllegalArgumentException();
        }
    }

    /**
     * Returns the last component in the path.
     *
     * @return the last element in a path
     * @throws IllegalArgumentException If the path represents the root
     *                                  directory, and therefore has no last
     *                                  component.
     */
    public String last() {
        if (!isRoot()) {
            return pathVector.lastElement();
        } else {
            throw new IllegalArgumentException("The path is the root, and has no last element");
        }
    }

    /**
     * Determines if the given path is a subpath of this path.
     *
     * <p>
     * The other path is a subpath of this path if is a prefix of this path.
     * Note that by this definition, each path is a subpath of itself.
     *
     * @param other The path to be tested.
     * @return <code>true</code> If and only if the other path is a subpath of
     * this path.
     */
    public boolean isSubpath(Path other) {
        if (other.isRoot()) {
            return true;
        } else if (other.pathVector.size() > pathVector.size()) {
            return false;
        } else {
            for (int i = 0; i < other.pathVector.size(); i++) {
                if (!pathVector.elementAt(i).equals(other.pathVector.elementAt(i)))
                    return false;
            }
            return true;
        }
    }

    /**
     * Converts the path to <code>File</code> object.
     *
     * @param root The resulting <code>File</code> object is created relative
     *             to this directory.
     * @return The <code>File</code> object.
     */
    public File toFile(File root) {
        return new File(root, toString());
    }


    /**
     * Compares two paths for equality.
     *
     * <p>
     * Two paths are equal if they share all the same components.
     *
     * @param other The other path.
     * @return <code>true</code> if and only if the two paths are equal.
     */
    @Override
    public boolean equals(Object other) {
        if (other instanceof Path) {
            return toString().equals(other.toString());
        }
        return false;
    }

    /**
     * Returns the hash code of the path.
     */
    @Override
    public int hashCode() {
        return toString().hashCode();
    }

    /**
     * Converts the path to a string.
     *
     * <p>
     * The string may later be used as an argument to the
     * <code>Path(String)</code> constructor.
     *
     * @return The string representation of the path.
     */
    @Override
    public String toString() {
        if (!pathVector.isEmpty()) {
            StringBuilder result = new StringBuilder();
            for (String folderName : pathVector) {
                result.append("/").append(folderName);
            }
            return result.toString();
        } else {
            return "/";
        }
    }


    /**
     * Class providing a vector iterator that does not allow removal
     */
    public static class VectorIterator implements Iterator<String>, Serializable {
        private Iterator<String> iterator;

        /**
         * Iterator constructor
         *
         * @param it the iterator
         */
        VectorIterator(Iterator<String> it) {
            iterator = it;
        }

        @Override
        public boolean hasNext() {
            return iterator.hasNext();
        }

        @Override
        public String next() {
            return iterator.next();
        }

        @Override
        public void remove() {
            throw new UnsupportedOperationException();
        }

    }
}