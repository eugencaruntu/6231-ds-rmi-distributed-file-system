package rmi;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * The listening thread class in which the server is accepting client connections
 * For each accepted connection, a new client thread is launched to deal with the request/communication
 */
public class ListeningThread extends Thread {

    private Skeleton skeleton;
    private ServerSocket serverSocket;
    private Exception stopCause = null;

    /**
     * Constructor for the Listening thread
     *
     * @param skeleton     the skeleton
     * @param serverSocket the server socket
     */
    ListeningThread(Skeleton skeleton, ServerSocket serverSocket) {
        this.skeleton = skeleton;
        this.serverSocket = serverSocket;
    }

    public void run() {

        while (skeleton.isRunning() && !this.isInterrupted()) {

            try {
                Socket connection = serverSocket.accept();
                ClientThread clientThread = new ClientThread(connection, skeleton);
                clientThread.start();

            } catch (IOException e) {
                if (skeleton.isRunning() && skeleton.listen_error(e)) {
                    System.out.println(e.getMessage());
                } else {
                    skeleton.setIsRunning(false);
                    interrupt();
                    stopCause = e;
                }
            }
        }

        skeleton.stopped(stopCause);
    }

}
