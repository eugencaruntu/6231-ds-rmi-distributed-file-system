package rmi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.net.Socket;

import static rmi.DynamicMessage.DynamicMessageStatus.FAILURE;
import static rmi.DynamicMessage.DynamicMessageStatus.SUCCESS;

/**
 * The client thread class in which the client interacts with a remote server
 * It invokes a method and interprets the response
 */
public class ClientThread extends Thread {

    private Socket connection;
    private Skeleton skeleton;

    /**
     * Constructor for the client thread
     *
     * @param connection the socket
     * @param skeleton   the skeleton
     */
    ClientThread(Socket connection, Skeleton skeleton) {
        this.connection = connection;
        this.skeleton = skeleton;
    }

    public void run() {
        ObjectOutputStream outputStream;
        ObjectInputStream inputStream;
        DynamicMessage response;

        try {
            outputStream = new ObjectOutputStream(connection.getOutputStream());
            outputStream.flush();
            inputStream = new ObjectInputStream(connection.getInputStream());

            Object methodName = inputStream.readObject();
            Object parameterTypes = inputStream.readObject();
            Object returnType = inputStream.readObject();
            Object args = inputStream.readObject();

            Method serverMethod = skeleton.getClassInterface().getMethod((String) methodName, (Class[]) parameterTypes);

            if (!returnType.equals(serverMethod.getReturnType().getName())) {
                response = new DynamicMessage(FAILURE, new RMIException("ReturnType is incorrect"));
                outputStream.writeObject(response);
            }

            try {
                Object serverResponse = serverMethod.invoke(skeleton.getServer(), (Object[]) args);
                response = new DynamicMessage(SUCCESS, serverResponse);
                outputStream.writeObject(response);
            } catch (IllegalAccessException | IllegalArgumentException e) {
                response = new DynamicMessage(FAILURE, new RMIException(e.getCause()));
                outputStream.writeObject(response);
            } catch (InvocationTargetException e) {
                response = new DynamicMessage(FAILURE, e.getCause());
                outputStream.writeObject(response);
            }

            connection.close();
        } catch (ClassNotFoundException | NoSuchMethodException | SecurityException | IOException e) {
            skeleton.service_error(new RMIException(e.getCause()));
        }
    }
}
