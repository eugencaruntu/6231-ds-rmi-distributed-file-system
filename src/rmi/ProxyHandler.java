package rmi;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.io.Serializable;
import java.lang.reflect.InvocationHandler;
import java.lang.reflect.Method;
import java.net.InetSocketAddress;
import java.net.Socket;

import static rmi.DynamicMessage.DynamicMessageStatus.FAILURE;

/**
 * The class dealing with invocation of a method
 */
public class ProxyHandler implements InvocationHandler, Serializable {

    private String hostname;
    private int port;
    private InetSocketAddress address;
    private Class classInterface;

    /**
     * Constructor
     * Do not match naming with attributes of this object!
     *
     * @param c       the Interface to be implemented dynamically
     * @param address the socket where this service is offered
     */
    ProxyHandler(InetSocketAddress address, Class c) {
        this.hostname = address.getHostName();
        this.port = address.getPort();
        this.address = address;
        this.classInterface = c;
    }

    public Object invoke(Object proxy, Method method, Object[] args) throws Exception {

        // ################################### //
        //       HANDLE REMOTE METHODS
        // ################################### //

        if (method.getName().equals("toString") && method.getReturnType().getName().equals("java.lang.String") && method.getParameterTypes().length == 0) {
            ProxyHandler r = (ProxyHandler) java.lang.reflect.Proxy.getInvocationHandler(proxy);
            return r.classInterface.getName() + " " + r.address.toString();
        }

        if (method.getName().equals("hashCode") && method.getReturnType().getName().equals("int") && method.getParameterTypes().length == 0) {
            ProxyHandler r = (ProxyHandler) java.lang.reflect.Proxy.getInvocationHandler(proxy);
            return r.classInterface.hashCode() * r.address.hashCode();
        }

        if (method.getName().equals("equals") && method.getReturnType().getName().equals("boolean") && method.getParameterTypes().length == 1) {
            if (args.length != 1 || args[0] == null) {
                return false;
            }
            ProxyHandler first = (ProxyHandler) java.lang.reflect.Proxy.getInvocationHandler(proxy);
            ProxyHandler second = (ProxyHandler) java.lang.reflect.Proxy.getInvocationHandler(args[0]);
            return first.classInterface.equals(second.classInterface) && first.address.equals(second.address);
        }

        // ################################### //
        //       HANDLE REMOTE METHODS
        // ################################### //

        Socket connection;
        DynamicMessage response;

        try {
            connection = new Socket(hostname, port);
            ObjectOutputStream outputStream = new ObjectOutputStream(connection.getOutputStream());
            outputStream.flush();
            ObjectInputStream inputStream = new ObjectInputStream(connection.getInputStream());

            outputStream.writeObject(method.getName());
            outputStream.writeObject(method.getParameterTypes());
            outputStream.writeObject(method.getReturnType().getName());
            outputStream.writeObject(args);

            response = (DynamicMessage) inputStream.readObject();
            connection.close();

        } catch (IOException | ClassNotFoundException e) {
            throw new RMIException(e.getCause());
        }

        if (response.status() == FAILURE)
            throw (Exception) response.getPayload();

        return response.getPayload();
    }

}
