package rmi;

import java.io.Serializable;


/**
 * A class to consolidate the method request/response communication both ways
 */
public class DynamicMessage implements Serializable {
    private DynamicMessageStatus header;
    private Object payload;

    /**
     * Possible status for a remote method invocation
     */
    public enum DynamicMessageStatus {SUCCESS, FAILURE}

    /**
     * Constructor for skeleton responses
     *
     * @param header  the message for the received
     * @param payload the result of the method being invoked
     */
    DynamicMessage(DynamicMessageStatus header, Object payload) {
        this.header = header;
        this.payload = payload;
    }

    /**
     * @return the payload which is usually an exception
     */
    Object getPayload() {
        return payload;
    }

    /**
     * @return the header representing the status of the remote method invocation
     */
    DynamicMessageStatus status() {
        return header;
    }

}
