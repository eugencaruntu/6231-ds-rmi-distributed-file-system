package rmi;

import java.lang.reflect.Method;
import java.net.InetAddress;
import java.net.UnknownHostException;

/**
 * Utility class to reuse shared methods
 */
class Util {


    /**
     * Check if the class represents a remote interface
     *
     * @param c   the class
     * @param <T> the type
     */
    static <T> void remoteInterfaceCheck(Class<T> c) {
        if (!remoteInterface(c)) {
            throw new Error(String.format("The %s is not a remote Interface.", c.getCanonicalName()));
        }
    }

    /**
     * Helper method to determine if a class is an interface and if all its methods throw RMIException
     *
     * @param c the class
     * @return true if a remote interface, false otherwise
     */
    static boolean remoteInterface(Class c) {

        if (!c.isInterface()) {
            return false;
        }
        boolean isRemoteInterface = false;
        for (Method method : c.getDeclaredMethods()) {
            isRemoteInterface = false;
            for (Class exception : method.getExceptionTypes()) {
                if (exception.getName().contains("RMIException")) {
                    isRemoteInterface = true;
                    break;
                }
            }
        }
        return isRemoteInterface;
    }

    /**
     * Check for null arguments
     *
     * @param c      the class
     * @param object any object
     * @param <T>    the type
     * @throws NullPointerException when either arguments are null
     */
    static <T> void nullArgumentsCheck(Class<T> c, Object object) throws NullPointerException {
        if (c == null || object == null) throw new NullPointerException("Null arguments are not supported.");
    }

    /**
     * Check for null arguments
     *
     * @param c       the class
     * @param object1 any first object
     * @param object2 any second object
     * @param <T>     the type
     * @throws NullPointerException when either arguments are null
     */
    static <T> void nullArgumentsCheck(Class<T> c, Object object1, Object object2) throws NullPointerException {
        if (c == null || object1 == null || object2 == null)
            throw new NullPointerException("Null arguments are not supported.");
    }

    /**
     * Check if skeleton is started
     *
     * @param skeleton the skeleton
     * @param <T>      the type
     * @throws IllegalStateException if the skeleton is not running
     */
    static <T> void skeletonStartedCheck(Skeleton<T> skeleton) throws IllegalStateException {
        if (skeleton.getHostName() == null || !skeleton.isRunning())
            throw new IllegalStateException("The skeleton is not running.");
    }

    /**
     * Check for local host address assignment and throw exception if not
     *
     * @return the host name
     * @throws UnknownHostException when no address can be determined
     */
    static String localHostAddressCheck() throws UnknownHostException {
        if (InetAddress.getLocalHost().getHostAddress() == null)
            throw new UnknownHostException("No address found for the local host.");
        return InetAddress.getLocalHost().getHostAddress();
    }

    /**
     * Check if skeleton has no port and return it or throw exception
     *
     * @param skeleton the skeleton
     * @param <T>      the type
     * @return the port
     * @throws IllegalStateException when no port or incorrect is assigned
     */
    static <T> int skeletonPortCheck(Skeleton<T> skeleton) throws IllegalStateException {
        int skeletonPort = skeleton.getPort();
        if (!(0 < skeletonPort && skeletonPort <= 65535)) {
            throw new IllegalStateException("The skeleton has no valid port assigned.");
        }
        return skeletonPort;
    }
}
